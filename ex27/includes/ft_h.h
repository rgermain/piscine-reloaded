/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_h.h                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/13 18:45:56 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/01 15:08:27 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FT_H_H
# define FT_H_H
# include <unistd.h>

void	ft_putchar(char c);
void	ft_putstr(char *str);
int		ft_display_file(char *argv);
int		main(int argc, char **argv);

#endif
