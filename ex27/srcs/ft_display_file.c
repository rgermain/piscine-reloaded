/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_display_file.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/15 11:38:36 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/01 15:09:50 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "ft_h.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int	ft_display_file(char *argv)
{
	int		file_text;
	int		file_op;
	char	file_ret[10 + 1];

	if ((file_op = open(argv, O_RDONLY)) == -1)
		return (0);
	while ((file_text = read(file_op, file_ret, 10)))
	{
		file_ret[file_text] = '\0';
		ft_putstr(file_ret);
	}
	close(file_op);
	return (0);
}
