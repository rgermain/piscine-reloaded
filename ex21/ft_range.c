/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_range.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/08 14:59:02 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/01 14:36:17 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int	len;
	int	a;
	int	*str;

	len = 0;
	a = 0;
	if (min >= max)
		return (NULL);
	str = (int*)malloc(sizeof(*str) * (max - min));
	while (min <= max)
	{
		str[a] = min;
		min++;
		a++;
	}
	return (str);
}
