/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_params.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: rgermain <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/08/07 13:54:46 by rgermain     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/01 14:21:31 by rgermain    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	int a;

	a = 0;
	while (str[a] != '\0')
		ft_putchar(str[a++]);
}

int		main(int argc, char **argv)
{
	int a;

	a = 1;
	if (argc >= 1)
		while (argv[a] != '\0')
		{
			ft_putstr(argv[a++]);
			ft_putchar('\n');
		}
	return (0);
}
